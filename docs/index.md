# KYPO Cyber Range Platform

KYPO Cyber Range Platform (KYPO CRP) is developed by Masaryk University since 2013, and it represents several years of our experience with cyber ranges in education, cyber defense exercises, and trainings. KYPO CRP is entirely based on state-of-the-art approaches such as containers, infrastructures as code, microservices, and open-source software, including cloud provider technology - OpenStack. With practical applications in mind, the team emphasized repeatability, scalability, automation, and interoperability to minimize human tasks and make cyber trainings affordable and cost-efficient.

KYPO CRP uses the same open approach for the content as for its architecture to encourage creating a community of trainers and supporting the sharing of training building blocks. For that reason, virtual machines, networks, and trainings are entirely defined in human-readable data-serialization languages JSON and YAML or use open-source software Packer to build virtual machines and Ansible for describing machine content.

The KYPO CRP sources are currently hosted at [https://gitlab.ics.muni.cz/muni-kypo-crp](https://gitlab.ics.muni.cz/muni-kypo-crp).

## Getting Started

We are glad that you are interested in our KYPO Cyber Range Platform. Please check the section Basic Concepts if you want to read more about the platform. It contains all supported [use-cases](./basic-concepts/use-cases.md) of the platform, used [terminology](./basic-concepts/terminology.md), and also the [typical workflow](./basic-concepts/typical-training-workflow/overview.md). Please also consult [Available Features](./available-features.md) for what is done for now and what is not.

If you decide to deploy KYPO CRP in your organization, please check our Installation Guide and pay attention to a page describing [OpenStack Requirements](./installation-guide/openstack-requirements.md). All information you need for work with our platform should be available in this documentation. If you think you missed something, please, check [FAQ](./faq.md) before contacting us.

## Be a part of our KYPO CRP community

KYPO Cyber Range Platform is an open-source project. We walked a long way with the support of projects and institutions listed below. If you are considering supporting our KYPO CRP, please [contact us](https://www.crp.kypo.muni.cz/#contact). On the other hand, if you are thinking about contributing to KYPO you can find more details at [Contribute to Docs](contribute-to-docs.md).

## Get Professional Support

We tried our best to create the KYPO Cyber Range Platform as simple as is possible for such complex software. We made all our documentation public here to help you with deployment and running the platform.

But if you need professional support or just looking for someone who will help you pick up the OpenStack cluster and deploy the KYPO CRP for you, [let us know](https://www.crp.kypo.muni.cz/#contact).
