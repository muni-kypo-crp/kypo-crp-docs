# Project Authors

## Active Core Contributors

| Surname | Name | Role |
| ------ | ------ | ------ |
| Krebs | Radovan | Python leader |
| Majdan | Lukáš | Java & Angular leader |
| Murár | Martin | Python developer |
| Murín | Mário | Java & Angular developer |
| Paluba | Juraj | Python developer |
| Pretschner | Robert | Python developer |
| Sapák | Tomáš | DevOps leader |

## Inactive Core Contributors

| Surname | Name | Role |
| ------ | ------ | ------ |
| *Andoniadis* | *Kamil* | *Python & DevOps leader* |
| *Duda* | *Jan* | *Java developer* |
| *Hai Duong* | *Tran* | *Angular developer* |
| *Hamerník* | *Martin* | *Angular leader* |
| *Ignác* | *Igor* | *Angular leader* |
| *Jaduš* | *Boris* | *Java developer* |
| *Juhás* | *Martin* | *Java developer* |
| *Dočkalová Burská* | *Karolína* | *Team leader & Visualization R&D* |
| *Kočamba* | *Erik* | *Angular developer* |
| *Ošlejšek* | *Radek* | *Visualisation R&D* |
| *Pilár* | *Dominik* | *Java & Angular developer* |
| *Rusňák* | *Vít* | *UI/UX R&D* |
| *Staněk* | *Miloslav* | *Python developer* |
| *Šeda* | *Pavel* | *Java & DB services leader* |
| *Tovarňák* | *Daniel* | *Technical leader* |
| *Urban* | *Michal* | *Python leader* |
| *Zbončáková* | *Tatiana* | *Python developer* |

## Advisory Board

| Surname | Name | Role |
| ------ | ------ | ------ |
| Čegan | Jakub | Project Management & Industrial Applications & Business Development |
| Čeleda | Pavel | KYPO Principal Investigator |
| Tovarňák | Daniel |  Architecture & Design |
| Vykopal | Jan | Educational research & User testing |

## Special Recognition

!!!note
    Here goes a list of people (surely not complete) whose invaluable help allowed us to make the current version of KYPO CRP into what it is today.

- Čermák Milan
- Čermák Roman
- Eichler Zdenek
- Hašák Šimon
- Horák Martin
- Kacvinský Tomáš
- Klimentová Lucie
- Košuth Jakub Bartolomej
- Kouřil Daniel
- Lago Dušan
- Laštovička Martin
- Macháč Martin
- Mařinec Petr
- Mojdlová Helena
- Nutár Ivo
- Plesník Tomáš
- Procházka Michal
- Račanský Václav
- Rebok Tomáš
- Sliacka Silvia
- Švábenský Valdemar
- Toth Dalibor
- Vizváry Martin
- Volf Daniel
- Vydra Zdeněk
