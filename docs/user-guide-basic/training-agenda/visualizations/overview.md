# Overview

The visualizations serve to grasp better the data collected during the training sessions. They can be divided into two types according to the type of the training:

1. **[Visualizations for *linear* training](visualizations-for-linear.md)** 
2. **[Visualizations for *adaptive* training](visualizations-for-adaptive.md)**

When considering the main user roles related to the usage of the visualizations, the *linear* games can be further divided as:

1. **[Visualizations for *instructors*](visualizations-for-linear.md#for-instructors)** are available either for viewing the ongoing training instances or their summary results.
2. **[Visualizations for *trainees*](visualizations-for-linear.md#for-trainees)** can be viewed by the trainees after they finish their training runs.

!!! note
    See the page [Roles](../../../user-guide-advanced/users-and-groups/roles.md) for more specific explanation of the roles and their privileges.
