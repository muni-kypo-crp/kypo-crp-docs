## Post-training adaptive instance simulator
This post-training instance simulator allows the instructor to analyze the finished **adaptive** trainings further. The instructor can also use this analysis's output to improve the analyzed **adaptive** definition for future trainings.

<p align="center">
  <img src="../../../../img/user-guide-basic/training-agenda/training-definition/instance-simulator/instance-simulator-edit.png">
</p>

The simulator can be accessed by clicking the ![instance-simulator-button](../../../img/buttons/instance-simulator-button.png) button, [Adaptive Training Definition Overview page](adaptive-training-definition.md#adaptive-training-definition-overview). Simulating tool works with the data exported from the previous instances.
!!! note
    The Instructor can find the option to export data from the desired instance on the [Training Instances Overview](../training-instance.md#training-instance-overview) page.

#### Adaptive Training Definition Panel
This panel allows the instructor to work further with the uploaded training definition. The instructor can adjust the decision matrix weights and the selected phase's restrictions. The tool also provides a read-only view of tasks assigned to the selected phase. Moreover, the pre-training questionnaire relations are displayed upon selecting the pre-training questionnaire phase.

#### Instance Simulation
After using the ![instance-simulator-button](../../../img/buttons/generate-button.png) button, the instructor can generate participants' performance on the edited training definition. This re-generates the sankey diagram that displays the task assignment of all participants. Before the generate button is pressed, this panel shows the original sankey diagram exported with the training instance data. More precisely, the pathway of every single trainee through the training.

<p align="center">
  <img src="../../../../img/user-guide-basic/training-agenda/training-definition/instance-simulator/instance-simulator-sankey.png">
</p>