# Overview
The purpose of the Sandbox Agenda is to create and manage sandbox definitions, pools, instances and resources. These activities are mapped onto the [Sandbox Creation](../../basic-concepts/typical-training-workflow/training-workflow-cloud.md#sandboxes-creation) workflow. User with role [instructor](../../user-guide-advanced/users-and-groups/roles.md#instructor) can access this agenda. The sandbox agenda is divided into three parts:

* **[Sandbox Definition](sandbox-definition.md)** section is used to create Sandbox definitions.

* **[Pool](pool.md)** section is used to create Pools from Sandbox definitions and manage created Pools. It also displays available Instances, vCPUs, and RAM in cloud service.

* **[Images](images.md)** section provides a list of available OS images.

To access the above-mentioned pages, click the respective button on the front page of the KYPO portal. 

<p align="center">
  <img src="../../../img/user-guide-basic/sandbox-agenda/overview/home-page-sandbox-definition-button.png">  <img src="../../../img/user-guide-basic/sandbox-agenda/overview/home-page-pool-button.png">  <img src="../../../img/user-guide-basic/sandbox-agenda/overview/home-page-resources-button.png">
</p>

Or by clicking the respective button in the global navigation in the section **Sandboxes**:

<p align="center">
  <img src="../../../img/user-guide-basic/sandbox-agenda/overview/sandbox-agenda-overview-panel.png">
</p>

