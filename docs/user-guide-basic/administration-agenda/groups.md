This page is used to list all groups created in the KYPO platform. Groups are essential to the user-access, as the access roles are not assigned to specific users but groups. The user acquires access to the KYPO portal agendas based on the roles of the groups they belong to. Each group also has a separate detail page containing additional information about it. This page can be accessed upon clicking on the group's name.

![group-overview](../../img/user-guide-basic/administration-agenda/groups/group-overview.png) 

To create a new group, the administrator must click on ![create-button](../../img/buttons/create-button.png) button that redirects them to the page [Create Group](#create-or-edit-group). The last column of the table contains actions :material-pencil:{: .icon .blue } &nbsp; :material-delete:{: .red .icon }: 


??? pencil "Edit"
    
    Click the button will redirect to the [Edit Group](#create-or-edit-group) page.
    
??? trash-can "Delete"
    
    Click the button, the following confirmation window will be opened: 
    
    <p align="center">
      <img src="../../../img/user-guide-basic/administration-agenda/groups/delete-confirmation.png">
    </p>
    
    After confirming, the group will be deleted from the KYPO portal.
    
    !!! info
        Multiple groups can be deleted by checking groups with checkboxes situated on the left side of the table row and by clicking on ![delete-button](../../img/buttons/big-delete-button.png) button.

## Create or Edit Group

The page consists of three panels, which allow to [create/edit group](#1-createedit-group), [add/edit members](#2-edit-members) of the group, and [edit roles](#3-edit-roles) of the group.

![create-group](../../img/user-guide-basic/administration-agenda/groups/create-group-all.png)

!!! note 
    Create Group and Edit Group pages are the same but the Edit Group page has some fields pre-filled. 

During the creation of a group, the second and third panels are disabled. To make them accessible, the administrator must fill the required fields in the first panel and either click on: 

1. ![create-button](../../img/buttons/create-button.png) that will create the new group and redirect the administrator back to the **Group Overview**.
2. ![create-and-edit-button](../../img/buttons/create-and-continue-button.png) that will stay on the page and allow the administrator to edit **members** and **roles** of the group.

### 1. Create/Edit Group
Here, the administrator is required to fill up all the necessary fields before creating a new group. The `Expiration Date` field is optional and is useful in the cases when the administrator will create a group of instructors, e.g., for a particular semester. After filling up fields, click on the ![create-button](../../img/buttons/create-button.png) button to save the group. Then panels **Edit members** and **Edit roles** will be rolled down. 

![create-group-panel](../../img/user-guide-basic/administration-agenda/groups/create-group.png)
### 2. Edit Members
Here, the administrator can add users individually or import all users from the group. The part **Add users** provides an easy way to search for users whose administrator wants to add to the group. Administrators can also add all users from the specific group in the part **Import users**. Click on the ![add-button](../../img/buttons/add-button.png) button to add selected users to the group. All users will be added to the group and will be present in the list in the part **Members of group**. A user or multiple users can be removed via the common way mentioned before.

![edit-members-panel](../../img/user-guide-basic/administration-agenda/groups/edit-members.png)
### 3. Edit Roles 
Here, the administrator can assign roles to the group. The part **Add roles** provide an easy way to search for roles which the administrator would like to assign to the group. Click on the ![add-button](../../img/buttons/add-button.png) button to assign all selected roles to the group. Assigned roles will be present in the list in the part **Roles of group**. A role or multiple roles can be removed via the common way mentioned before.

![edit-members-panel](../../img/user-guide-basic/administration-agenda/groups/edit-roles.png)

## Group Detail

The page consists of three panels. The first panel displays the group description, expiration date, number of users in the group, and the number of roles that the group has. The second panel shows the list of users in the group and the basic information about them.

![group-detail](../../img/user-guide-basic/administration-agenda/groups/group-detail.png)

The last panel shows all roles assigned to the group. Every role also has expandable detail upon clicking on the :material-chevron-down:{: .grey .icon }. This detail displays the role description.

![group-detail-role-detail](../../img/user-guide-basic/administration-agenda/groups/group-role-detail.png)
