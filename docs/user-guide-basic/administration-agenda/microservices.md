## Microservice Overview
This page lists all microservices that are registered in the KYPO Platform. A new microservice can be added to the platform by clicking the ![register-button](../../img/buttons/register-button.png) in the top right corner that will redirect an administrator to the [Microservice Registration](#microservice-registration) page. 

![microservice-page](../../img/user-guide-basic/administration-agenda/microservices/microservice-overview.png)

## Microservice Registration
This page is used to register a new microservice, which is then used in the platform's background. The recommended way is to import the [kypo-security-commons library](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-security-commons) into service and register microservice during startup.

![microservice-page](../../img/user-guide-basic/administration-agenda/microservices/microservice-registration.png)
