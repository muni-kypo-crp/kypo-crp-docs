# What's done 

* [x] Basic user management
* [x] Basic sandbox management
* [x] Support for simple network topologies (hierarchical, acyclic)
* [x] Advanced management of trainings
* [x] Total isolation of *SSH jump-host tunnels* for trainee access ([More information](./user-guide-advanced/sandboxes/sandbox-access.md))

# What's not done

* [ ] Support for complex network topologies (e.g., multi-route or cyclic networks)
* [ ] Consolidated debug logging (debug logs are not in one place)

!!! warning
    Some of the unfinished features prohibit the use of the platform in unrestricted and untrusted environments. Use carefully in production.




