# Overview

The **User Guide (Advanced)** objective is to provide a detailed description of some KYPO Cyber Range Platform areas to the users who intend to gain advanced knowledge about the platform. The section is divided into three parts: 

1. **[Sandboxes:](sandboxes/sandboxes-overview.md)** Detailed description of sandbox structure, how it works internally, or the way how to connect to the sandbox using SSH, SPICE client or Apache Guacamole.
2. **[Trainings:](trainings/trainings-overview.md)** Detailed description of the trainings, how the training entities are interconnected, and the way how they are interconnected with sandboxes. 
3. **[Users and Groups:](users-and-groups/users-and-groups-overview.md)** The section describes the permission system used in KYPO CRP and the meaning of the user rights.
