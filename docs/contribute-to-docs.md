Before contacting us, please read the following steps describing different ways how to contribute to the documentation of the KYPO CRP.

* Contributing to documentation can be done after creating an issue/branch. You should create this issue or branch under the [project](https://gitlab.ics.muni.cz/muni-kypo-crp/kypo-crp-docs) that stands behind this documentation.
* Changes to documentation can be of various sizes, but always keep in mind to separate more extensive changes across multiple pages to several issues.
* If you are unsure if you should include your change in the documentation, don't hesitate to get in touch with us first on *support@kypo.cz*.
* While contributing to project documentation keep in mind that you should keep your text as clear and understandable as possible.
* You can run documentation locally as it is shown in Gitlab [README](https://gitlab.ics.muni.cz/muni-kypo-crp/kypo-crp-docs). Furthermore, you can find the syntax and functionality of MkDocs in this [documentation](https://docs.crp.kypo.muni.cz/extras/writing-documentation-mkdocs/).
* When your work is ready, create a merge request. You don't have to assign anybody as an assignee because a member of the project will assign themself. Your changes will be reviewed and, if necessary, adjusted a bit.
