Before starting a training run, several steps must be performed. These steps are not necessarily performed by a single user and they are usually divided among several users with a role [instructor](../../user-guide-advanced/users-and-groups/roles.md#instructor). The whole workflow of creating training depends on the type of used sandboxes (cloud or local). In general, workflow is divided into two parts, as it also includes the creation of the sandboxes.

1. **Sandboxes Creation**
    a) [Cloud sandboxes](training-workflow-cloud.md#sandboxes-creation)
    b) [Local sandboxes](training-workflow-local.md#sandboxes-creation)
2. **Training Creation**
    a) [Training with cloud sandboxes (cloud environment)](training-workflow-cloud.md#training-creation)
    b) [Training with local sandboxes (local environment)](training-workflow-local.md#training-creation) 

Several steps of both approaches overlap. Above all, the creation of training and sandbox definitions are the same. It allows instructors to reuse them for both use cases.

## Prerequisites
It is assumed that KYPO CRP is installed according to the [installation guide](../../installation-guide/installation-guide-overview.md) and created KYPO CRP instance is connected to the OpenStack cloud service, and through the respective microservice, it can create and manage sandboxes inside the cloud via available APIs.





