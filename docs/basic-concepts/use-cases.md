The result of the project KYPO CRP is the platform that focuses on two use cases:

1. **Emulated Virtual Environment**: Creating and providing an environment to emulate computer infrastructures in a controlled environment to achieve high flexibility, scalability, isolation, and portability. The platform allows you to create virtual networks with a full-fledged operating system and network devices that emulate real-world systems.
2. **Trainings**: Running simultaneous training sessions as cybersecurity games complemented by an assessment of participants. KYPO CRP enables instructors to create training scenarios and learners to engage in the training sessions featuring an emulated virtual environment provided by KYPO CRP. Instructors can easily create multiple instances of the same training and virtual environment for tens and hundreds of participants via the web interface and monitor their progress at the dashboard.


In addition, KYPO CRP provides a **User Management**. It is an essential part of the platform because it allows you to restrict access to the trainings and emulated virtual environments.
